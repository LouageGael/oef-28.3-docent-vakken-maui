﻿
namespace MauiApp_oef28._3;

public partial class MainPage : ContentPage
{
    private List<Vak> lijstVakken = new List<Vak>()
    {
        new Vak("Basis C#", "Lok 1", 5),
        new Vak("Advanced c#", "Lok 1", 2),
        new Vak("GIT","Lok 5", 4),
        new Vak("Web", "Lok 8", 10)
    };
    private List<Docent> DocentList = new List<Docent>()
    {
        new Docent("Gael"){},
        new Docent("Benji"){},
        new Docent("Harald"){},
    };
	public MainPage()
	{
		InitializeComponent();

        pckDocent.ItemsSource = null;
        pckDocent.ItemsSource = DocentList;

    }
    private void OnBtnSluitenClicked(object sender, EventArgs e)
    {
        Environment.Exit(0);
    }
    private void OnBtnVakkenToevoegenClicked(object sender, EventArgs e)
    {
       if(!string.IsNullOrEmpty(txtBeschrijving.Text) && !string.IsNullOrEmpty(txtLesuren.Text) && pckLeslokaal.SelectedIndex != -1)
        {
            if(pckDocent.SelectedIndex != -1)
            {
                if(int.TryParse(txtLesuren.Text, out int lesuren))
                {
                    DocentList[pckDocent.SelectedIndex].AddVak(new Vak(txtBeschrijving.Text, pckLeslokaal.SelectedItem.ToString(), lesuren));
                    lvVakken.ItemsSource = null;
                    lvVakken.ItemsSource = DocentList[pckDocent.SelectedIndex].Vakken;
                }
            }
        }
    }

    private void OnBtnDocentToevoegenClicked(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDocentNaam.Text))
        {
            DocentList.Add(new Docent(txtDocentNaam.Text));
            pckDocent.ItemsSource = null;
            pckDocent.ItemsSource = DocentList;
            txtDocentNaam.Text = String.Empty;
        }
    }
    private void OnPickerDocentSelectionIndexChanged(object sender, EventArgs e)
    {
        var picker = (Picker)sender;
        int selectedIndex = picker.SelectedIndex;

        if (selectedIndex != -1)
        {
            lvVakken.ItemsSource = null;
            lvVakken.ItemsSource = DocentList[picker.SelectedIndex].Vakken;
        }
    }

    private void OnBtnVerwijderenClicked(object sender, EventArgs e)
    {
        if(pckDocent.SelectedIndex != -1)
        {
            if (lvVakken.SelectedItem != null)
            {
                Vak q = lvVakken.SelectedItem as Vak;
                DocentList[pckDocent.SelectedIndex].RemoveVak(q);
                lvVakken.ItemsSource = null;
                lvVakken.ItemsSource = DocentList[pckDocent.SelectedIndex].Vakken;
            }
        }
        
    }

}
class Docent
{
	private string _naam;
	private List<Vak> _vakken = new List<Vak>();

	public void AddVak(Vak vak)
    {
		Vakken.Add(vak);
    }
	public void RemoveVak(Vak vak)
    {
		Vakken.Remove(vak);
    }
    public override string ToString()
    {
        return base.ToString();	
    }
    public Docent(string naam)
    {
		Naam = naam;
    }
    public List<Vak> Vakken { get => _vakken; set => _vakken = value; }
    public string Naam { get => _naam; set => _naam = value; }
}
class Vak
{
	private string _beschrijving;
	private string _leslokaal;
	private int _lesuren;

    public Vak(string beschrijving, string leslokaal, int lesuren)
    {
        _beschrijving = beschrijving;
        _leslokaal = leslokaal;
        _lesuren = lesuren;
    }
    public override string ToString()
    {
        return base.ToString();
    }
    public override bool Equals(Object obj)
    {
        //Check for null and compare run-time types.
        if ((obj == null) || !this.GetType().Equals(obj.GetType()))
        {
            return false;
        }
        else
        {
            Vak p = (Vak)obj;
            return Beschrijving == p.Beschrijving;
        }
    }
    public string Beschrijving { get => _beschrijving; set => _beschrijving = value; }
    public string Leslokaal { get => _leslokaal; set => _leslokaal = value; }
    public int Lesuren { get => _lesuren; set => _lesuren = value; }
}

